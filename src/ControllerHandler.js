module.exports = (options = {}) => {
  return (func) => {
    return async (req, res, next) => {
      try {
        return await func(req, res, next);
      } catch (error) {
        const {
          name, message, stack, httpStatus,
        } = error;
  
        const jsonToReturn = {};
  
        jsonToReturn[options.customKey || 'error'] = message
        if(options.showNameError && name){
          jsonToReturn.name = name
        }
  
        if (httpStatus) { return res.status(httpStatus).json(jsonToReturn); }

        jsonToReturn.stack = stack
  
        console.log('------------UNHANDLED ERROR------------');
        console.log(error);
        console.log('---------------------------------------');
        if (process.env.development || process.env.ENVIRONMENT === 'development') {
          return res.status(500).json(jsonToReturn);
        }
        jsonToReturn
        return res.status(500).json({ error: 'Server error' });
      }
    };
  };
  
}
