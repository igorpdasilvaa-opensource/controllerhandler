const res = require('./Controller.mock');
const Unbreaker = require('../src/ControllerHandler')();

test('Should handle the error and return status 400 and message: Name is required', async () => {
  delete process.env.development;

  const middlewareReturned = await Unbreaker(() => {
    throw { name: 'ValidationError', message: 'Name is required', httpStatus: 400 };
  });
  const result = await middlewareReturned(null, res, null);
  expect(result.status).toBe(400);
  expect(result.body.error).toBe('Name is required');
  expect(result.body.stack).toBeUndefined();
});

test('Must handle the error and return status 500, the message must be: Server error', async () => {
  delete process.env.development;
  const middlewareReturned = await Unbreaker(() => {
    krieegor.ThrowKrieegerIsNotDefined();
  });

  const result = await middlewareReturned(null, res, null);

  expect(result.status).toBe(500);
  expect(result.body.error).toBe('Server error');
  expect(result.body.stack).toBeUndefined();
});

test('Must handle the error and return status 500, and the message must be specified, bring a memory stack as well', async () => {
  process.env.development = true;

  const middlewareReturned = await Unbreaker(() => {
    krieegor.ThrowKrieegerIsNotDefined();
  });

  const result = await middlewareReturned(null, res, null);

  expect(result.status).toBe(500);
  expect(result.body.error).toBe('krieegor is not defined');
  expect(result.body.stack).not.toBeUndefined();
});

test('Checking async support --> Should handle the error and return status 400 and message: Name is required', async () => {
  delete process.env.development;
  const middlewareReturned = await Unbreaker(async () => {
    throw { name: 'ValidationError', message: 'Name is required', httpStatus: 400 };
  });
  const result = await middlewareReturned(null, res, null);
  expect(result.status).toBe(400);
  expect(result.body.error).toBe('Name is required');
  expect(result.body.stack).toBeUndefined();
});

test('Checking async support --> Must handle the error and return status 500, the message must be: Server error', async () => {
  delete process.env.development;

  const middlewareReturned = await Unbreaker(async () => {
    krieegor.ThrowKrieegerIsNotDefined();
  });

  const result = await middlewareReturned(null, res, null);

  expect(result.status).toBe(500);
  expect(result.body.error).toBe('Server error');
  expect(result.body.stack).toBeUndefined();
});

test('Checking async support --> Must handle the error and return status 500, and the message must be specified, bring a memory stack as well', async () => {
  process.env.development = true;

  const middlewareReturned = await Unbreaker(async () => {
    krieegor.ThrowKrieegerIsNotDefined();
  });

  const result = await middlewareReturned(null, res, null);

  expect(result.status).toBe(500);
  expect(result.body.error).toBe('krieegor is not defined');
  expect(result.body.stack).not.toBeUndefined();
});

test('Checking async support --> Must execute and call next function', async () => {
  process.env.development = true;

  const middlewareReturned = await Unbreaker(async (req, res, next) => next());

  const result = await middlewareReturned(null, res, () => true);

  expect(result).toBe(true);
});
