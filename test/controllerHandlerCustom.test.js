const res = require('./Controller.mock');
const Unbreaker = require('../src/ControllerHandler')({ showNameError: true, customKey: 'message' });

test('Should handle the error and return status 400 and message: Name is required', async () => {
  delete process.env.development;

  const middlewareReturned = await Unbreaker(() => {
    throw { name: 'ValidationError', message: 'Name is required', httpStatus: 400 };
  });
  const result = await middlewareReturned(null, res, null);
  expect(result.status).toBe(400);
  expect(result.body.message).toBe('Name is required');
  expect(result.body.stack).toBeUndefined();
});
