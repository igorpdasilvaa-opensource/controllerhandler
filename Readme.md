# What is this ? 
I was maked to handle error ocurred in controller preventing the whole back-end to break, i will handle the error and send a response in the requisition informing the error
# How install
Just copy and paste the code below
```
npm i -S @igorpdasilvaa/controller-handler
```
# How use
This handler is maked to use with express framework, just put the code below on your router and be happy
```
[.... other imports here ...]
const controllerHandler = require('@igorpdasilvaa/controller-handler')();
[............................]
router.post('/', controllerHandler(myController.myFunction);
```
# Options to make you form validator more custom
You can pass options in the moment you are instancing the const controller, the option available is `customKey`, this option will allow you to modify the key of object returned, the default it's `error`, so you can pass this parameter as below
```
const controllerHandler = require('@igorpdasilvaa/controller-handler')({customKey: 'message'});
```